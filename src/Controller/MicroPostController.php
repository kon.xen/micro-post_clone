<?php

namespace App\Controller;

use App\Entity\MicroPost;
use App\Entity\User;
//use App\controller\FollowingController;
use App\Form\MicroPostType;
use App\Repository\MicroPostRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use twig_Environment;


/**
 *@Route("/micro-post")
 */

class MicroPostController {
	/**
	 *@var Twig_environment
	 */
	private $twig;

	/**
	 *@var MicroPostRepository
	 */
	private $microPostRepository;

	/**
	 *@var UserRepository
	 */
	private $userRepository;

	/**
	 * @var FormFactoryInterface
	 */
	private $formFactory;

	/**
	 * @var EntityManagerInterface
	 */
	private $entityManager;

	/**
	 * @var RouterInterface
	 */
	private $router;

	/**
	 * @var FlashBagInterface
	 */
	private $flashBag;

	/**
	 * @var AuthorizationCheckerInterface
	 */
	private $authorizationChecker;

    /**
     * @param twig_Environment $twig
     * @param MicroPostRepository $microPostRepository
     * @param UserRepository $userRepository
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param RouterInterface $router
     * @param FlashBagInterface $flashBag
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
	public function __construct(
		Twig_Environment $twig,
		MicroPostRepository $microPostRepository,
		UserRepository $userRepository,
		FormFactoryInterface $formFactory,
		EntityManagerInterface $entityManager,
		RouterInterface $router,
		FlashBagInterface $flashBag,
		AuthorizationCheckerInterface $authorizationChecker
	) {

		$this->twig                 = $twig;
		$this->microPostRepository  = $microPostRepository;
		$this->userRepository       = $userRepository;
		$this->formFactory          = $formFactory;
		$this->entityManager        = $entityManager;
		$this->router               = $router;
		$this->flashBag             = $flashBag;
		$this->authorizationChecker = $authorizationChecker;
	}

    /**
     * @Route("/", name="micro_post_index")
     * @param TokenStorageInterface $tokenStorage
     * @param UserRepository $userRepository
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
	public function index(TokenStorageInterface $tokenStorage, UserRepository $userRepository): Response
    {
		$currentUser   = $tokenStorage->getToken()->getUser();
		$usersToFollow = [];

		if ($currentUser instanceof User){
			$posts = $this->microPostRepository->findAllByUsers(
				$currentUser->getFollowing()
			);

			$usersToFollow = count($posts) === 0?
			$userRepository->findAllWithMoreThan5PostsExceptUser($currentUser): [];

		} else {

			$posts = $this->microPostRepository->findby(
				[],
				['time' => 'DESC']
			);
		}

		$html = $this->twig->render(
			'micro-post/index.html.twig',
			[
				'posts' 		=> $posts,
				'usersToFollow' => $usersToFollow
			]
		);

		return new Response($html);
	}

	/**
	 *@Route("/edit/{id}", name="micro_post_edit")
	 *@Security("is_granted('edit', microPost)", message="Access denied")
	 */
	//Using param converter used in the argument of the method that uses the route to define the micropost id.
	public function edit(MicroPost $microPost, Request $request) 
	{
		//$this->denyUnlessGranted('edit', $microPost); --Requires the controller to extend symfony bass Controller--

		$form = $this->formFactory->create(MicroPostType::class , $microPost);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->entityManager->persist($microPost);
			$this->entityManager->flush();

			return new RedirectResponse(
				$this->router->generate('micro_post_index')
			);
		}

		return new Response(
			$this->twig->render(
				'micro-post/add.html.twig',
				['form' => $form->createView()]
			)
		);
	}

	/**
	 *@Route("/delete/{id}", name="micro_post_delete")
	 *@Security("is_granted('delete', microPost)", message="Access denied")
	 */
	public function delete(MicroPost $microPost) 
	{
		$this->entityManager->remove($microPost);
		$this->entityManager->flush();

		$this->flashBag->add('notice', 'Micro Post was deleted');

		return new RedirectResponse(
			$this->router->generate('micro_post_index')
		);
	}

    /**
     * @Route("/add", name="micro_post_add")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @return RedirectResponse|Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
	public function add(Request $request, TokenStorageInterface $tokenStorage) 
	{
		$user = $tokenStorage->getToken()->getUser();

		$microPost = new MicroPost();
		$microPost->setUser($user);

		$form = $this->formFactory->create(MicroPostType::class , $microPost);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->entityManager->persist($microPost);
			$this->entityManager->flush();

			$this->flashBag->add('notice', 'Micro Post created');

			return new RedirectResponse(
				$this->router->generate('micro_post_index')
			);
		}

		return new Response(
			$this->twig->render('micro-post/add.html.twig', ['form' => $form->createView()])
		);
	}
	/**
	 * @Route ("/user/{username}", name="micro_post_user")
	 */
	public function userPosts(User $userWithPosts) 
	{
		$html = $this->twig->render(
			'micro-post/user-posts.html.twig',
			[
				'posts'  => $this->microPostRepository->findby(
					['user' => $userWithPosts],
					['time' => 'DESC']
				),
				'user' => $userWithPosts,
				// or can do it like this
				// 'posts'=> $userWithPosts->getPosts()
			]
		);

		return new Response($html);
	}

    /**
     * @Route("/{id}", name="micro_post_post")
     * @param MicroPost $post
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
	public function post(MicroPost $post): Response
    {
		//line bellow not neede becasuse of param converter used in the argument above.
		//$post = $this->microPostRepository->find($id);

		return new Response(
			$this->twig->render('micro-post/post.html.twig', ['post' => $post])
		);
	}

}
