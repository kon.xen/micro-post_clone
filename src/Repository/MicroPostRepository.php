<?php

namespace App\Repository;

use App\Entity\MicroPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
//use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
/**
 * @method MicroPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method MicroPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method MicroPost[]    findAll()
 * @method MicroPost[]    findBy(array $criteria, array $orderBy = null, $limit
 *         = null, $offset = null)
 */

class MicroPostRepository extends ServiceEntityRepository {
	public function __construct(\Doctrine\Common\Persistence\ManagerRegistry $registry) {
		parent::__construct(
			$registry,
			MicroPost::class
		);
	}

	public function findAllByUsers($userIds) {

		$qb = $this->createQueryBuilder('p');

		return $qb->select('p')
		          ->innerJoin(
			'App\Entity\User',
			'u',
			'WITH',
			'p.user = u.id'
		)
		->where('u.id IN (:following)')
		->setParameter(
			'following',
			$userIds
		)
		->orderBy('p.time', 'DESC')
		->getQuery()
		->getResult();
	}
}
